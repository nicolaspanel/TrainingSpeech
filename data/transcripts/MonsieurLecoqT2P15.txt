Chapitre quinze de l'honneur du nom.
Deuxième partie de Monsieur Lecoq.
Cette enregistrement librivox fait partie du domaine public.
Monsieur Lecoq par Émile Gaboriau.
Deuxième partie l'honneur du nom.
Chapitre quinze.
Il n'y avait pas deux semaines que le duc de Sairmeuse était rentré en France, il n'avait pas encore eu le temps de secouer de ses souliers la poussière de l'exil, et déjà son imagination, troublée par la passion, lui montrait des ennemis partout.
Il n'était à Sairmeuse que depuis deux jours, et déjà il en était à accueillir sans discernement et de si bas qu'ils vinssent, les rapports envenimés qui caressaient ses rancunes.
Les soupçons qu'il eût voulu faire partager à Martial étaient cruellement et ridiculement injustes.
À l'heure même où il accusait le baron d'Escorval de tramer quelque chose, cet homme malheureux pleurait au chevet de son fils, qu'il croyait, qu'il voyait mourant...
Maurice était au moins en grand danger.
Son organisation nerveuse et impressionnable à l'excès, n'avait pu résister aux rudes assauts de la destinée, à ces brusques alternatives de bonheur sublimé et de désespoir qui se succédaient sans répit.
Quand, sur l'ordre si pressant de monsieur Lacheneur, il s'était éloigné précipitamment des bois de la Rèche, il avait comme perdu la faculté de réfléchir et de délibérer.
L'inexplicable résistance de Marie-Anne, les insultes du marquis de Sairmeuse, la feinte colère de Lacheneur, tout cela, pour lui, se confondait en un seul malheur, immense, irréparable, dont le poids écrasait sa pensée...
Les paysans qui le rencontrèrent, errant au hasard à travers les champs, furent frappés de sa démarche insolite, et pensèrent que sans doute une grande catastrophe venait de frapper la maison d'Escorval.
Quelques-uns le saluèrent...
il ne les vit pas.
Il souffrait atrocement.
Il lui semblait que quelque chose venait de se briser en lui, et il faisait à son énergie un appel désespéré.
Il essayait de s'accoutumer au coup terrible.
L'habitude—cette mémoire du corps qui veille alors que l'esprit s'égare—l'habitude seule le ramena à Escorval pour le dîner.
Ses traits étaient si affreusement décomposés que M
me
me
d'Escorval, en le voyant, fut saisie d'un pressentiment sinistre, et n'osa l'interroger.
Il parla le premier.
Tout est fini! prononça-t-il d'une voix rauque.
Mais ne t'inquiète pas, mère, j'ai du courage, tu verras...
Il se mit à table, en effet, d'un air assez résolu, il mangea presque autant que de coutume, et son père remarqua, sans mot dire, qu'il buvait son vin pur.
Tout en lui était si extraordinaire, qu'on l'eût dit animé par une volonté autre que la sienne, effet étrange et saisissant dont peuvent seuls donner l'idée, les mouvements inconscients d'une somnambule.
Il était fort pâle, ses yeux secs brillaient d'un éclat effrayant, son geste était saccadé, sa voix brève.
Il parlait beaucoup, et même il plaisantait...
Cherchait-il à s'étourdir?...
Que ne pleure-t-il! pensait M
me
me
d'Escorval épouvantée, je ne craindrais pas tant, et je le consolerais...
Ce fut le dernier effort de Maurice, il regagna sa chambre, et quand sa mère, qui était venue à diverses reprises écouter à sa porte, se décida à entrer vers minuit, elle le trouva couché, balbutiant des phrases incohérentes...
Elle s'approcha...
Il ne parut pas la reconnaître ni seulement la voir.
Elle lui parla...
Il ne sembla pas l'entendre.
Il avait la face congestionnée, les lèvres sèches, et par moments il sortait de sa gorge comme un râle.
Elle lui prit la main...
Cette main était brûlante.
Et cependant il grelottait, ses dents claquaient...
Un nuage passa devant les yeux de la pauvre femme, elle crut qu'elle allait se trouver mal; mais elle dompta cette faiblesse et se traîna jusque sur le palier, où elle cria.
Au secours!...
mon fils se meurt!
D'un bond, Monsieur d'Escorval fut à la chambre de Maurice.
Il regarda, comprit et se précipita dehors en appelant son domestique d'une voix terrible.
Attèle le cabriolet, lui ordonna-t-il, galope jusqu'à Montaignac et ramène un médecin...
crève le cheval plutôt que de perdre une minute!...
Il y avait bien un docteur à Sairmeuse, mais c'était le plus borné des hommes.
C'était un ancien chirurgien militaire, renvoyé de l'armée pour son incurable incapacité; on le nommait Rublot.
Il se soûlait, et quand il était ivre, il aimait à montrer une immense trousse pleine d'instruments effrayants, avec lesquels autrefois, sur les champs de bataille, il coupait, disait-il, les jambes comme des raves.
Les paysans le fuyaient comme la peste.
Quand ils étaient malades, ils envoyaient quérir le curé.
Monsieur d'Escorval fit comme les paysans, après avoir calculé que le médecin ne pouvait arriver avant le jour.
L'abbé Midon n'avait jamais fréquenté les écoles de médecine; mais au temps où il n'était que vicaire, les pauvres venaient si souvent lui demander conseil, qu'il s'était mis courageusement à l'étude, et que l'expérience aidant, il avait acquis un savoir que ne donne pas toujours le diplôme de la Faculté.
Quelle que fût l'heure à laquelle on vînt le chercher pour un malade, de jour ou de nuit, par tous les temps, on le trouvait prêt.
Il ne répondait qu'un mot.
Partons!
Et quand les gens des environs le rencontraient le long des chemins, avec son large chapeau et son grand bâton, sa boîte de médicaments pendue à l'épaule par une courroie, ils se découvraient respectueusement.
Ceux qui n'aimaient pas le prêtre estimaient l'homme.
Pour Monsieur d'Escorval, plus que pour tous les autres, l'abbé Midon devait se hâter.
Le baron était son ami.
C'est dire quelle appréhension le fit trembler, quand il aperçut, devant la grille, M
me
me
d'Escorval guettant son arrivée.
À la façon dont elle se précipita à sa rencontre, il crut qu'elle allait lui annoncer un malheur irréparable.
Mais non.
Elle lui prit la main, et sans prononcer une parole, elle l'entraîna jusqu'à la chambre de Maurice.
La situation de ce malheureux enfant était des plus graves, il ne fallut à l'abbé qu'un coup d'œil pour le reconnaître, mais elle n'était pas désespérée.
Nous le tirerons de là, dit-il avec un sourire qui ramenait l'espérance.
Et aussitôt, avec le sang-froid d'un vieux guérisseur, il pratiqua une large saignée et ordonna des applications de glace sur la tête et des sinapismes.
En un moment toute la maison fut en mouvement, pour accomplir ces prescriptions de salut.
Le prêtre en profita pour attirer le baron dans l'embrasure d'une fenêtre.
Qu'arrive-t-il donc?...
demanda-t-il.
Monsieur d'Escorval eut un geste désolé.
Un désespoir d'amour...
répondit-il.
monsieur Lacheneur m'a refusé la main de sa fille que je lui demandais pour mon fils...
Maurice a dû voir aujourd'hui Marie-Anne...
Que s'est-il passé entre eux?...
je l'ignore, vous voyez le résultat...
La baronne rentrait, les deux hommes se turent, et le silence vraiment funèbre de la chambre ne fut plus troublé que par les plaintes de Maurice.
Son agitation, loin de se calmer, redoublait.
Le délire peuplait son cerveau de fantômes, et à tout moment les noms de Marie-Anne, de Martial de Sairmeuse et de Chanlouineau revenaient dans ses phrases, trop incohérentes pour qu'il fût possible de suivre sa pensée.
Ce que cette nuit-là parut longue à Monsieur d'Escorval et à sa femme, ceux-là seuls le savent qui ont compté les secondes d'une minute près du lit d'un malade aimé...
Certes, leur confiance en l'abbé Midon, leur compagnon de veille, était grande; mais enfin, il n'était pas médecin, tandis que l'autre, celui qu'ils attendaient...
Enfin, comme l'aube faisait pâlir les bougies, on entendit au dehors le galop furieux d'un cheval, et peu après le docteur de Montaignac parut.
Il examina attentivement Maurice, et, après une courte conférence à voix basse avec le prêtre.
Je n'aperçois aucun danger immédiat, déclara-t-il.
Tout ce qu'il y avait à faire a été fait...
il faut laisser le mal suivre son cours...
je reviendrai.
Il revint en effet le lendemain et aussi les jours d'après, car ce ne fut qu'à la fin de la semaine suivante que Maurice fut déclaré hors de danger.
Ses parents remerciaient Dieu, lui s'affligeait.
Hélas! se disait-il, je souffrais moins quand je ne pensais pas.
Ce jour-là même, il raconta à son père toute la scène du bois de la Rèche, dont les moindres détails étaient restés profondément gravés dans sa mémoire.
Lorsqu'il eut terminé.
Tu es bien sûr, lui demanda son père, de la réponse de Marie-Anne?
Elle t'a bien dit que si son père donnait son consentement à votre mariage, elle refuserait le sien?...
Elle me l'a dit.
Et elle t'aime?
J'en suis sûr.
Tu ne t'es pas mépris au ton de monsieur Lacheneur, quand il t'a dit.
Mais va-t-en donc, petit malheureux!...
Non.
Monsieur d'Escorval demeura un moment pensif.
C'est à confondre la raison, murmura-t-il.
Et, si bas que son fils ne put l'entendre, il ajouta.
Je verrai Lacheneur demain, et il faudra bien que ce mystère s'explique.
fin du chapitre quinze de l'honneur du nom.
Enregistré par Ezwa en Belgique en janvier deux mille neuf.