Chapitre vingt-six de l'honneur du nom.
Deuxième partie de Monsieur Lecoq.
Cette enregistrement librivox fait partie du domaine public.
Monsieur Lecoq par Émile Gaboriau.
Deuxième partie l'honneur du nom.
Chapitre vingt-six.
Surtout, hâtez-vous! avait dit Maurice au messager qu'il chargeait de porter une lettre à sa mère.
Cet homme n'arriva pourtant à Escorval qu'à la nuit tombante.
Troublé par la peur, il s'était égaré à chercher des chemins de traverse, et il avait fait dix lieues pour éviter tous les gens qu'il apercevait, paysans ou soldats.
M
me
me
d'Escorval lui arracha la lettre des mains, plutôt qu'elle ne la prit.
Elle l'ouvrit, la lut à haute voix à Marie-Anne et n'ajouta qu'un seul mot.
Partons!
C'était plus aisé à dire qu'à exécuter.
Il n'y avait jamais eu que trois chevaux à Escorval; l'un était aux trois quarts mort de sa course furibonde de la veille; les deux autres étaient à Montaignac.
Comment faire?...
Recourir à l'obligeance des voisins était l'unique ressource.
Mais ces voisins, de braves gens d'ailleurs, qui avaient appris l'arrestation du baron, refusèrent bravement de prêter leurs bêtes.
Ils estimaient que ce serait se compromettre gravement que de rendre un service, si léger qu'il pût paraître, à la femme d'un homme sous le poids de la plus terrible des accusations.
M
me
me
d'Escorval et Marie-Anne parlaient déjà de se mettre en route à pied, quand le caporal Bavois, indigné de tant de lâcheté, jura par le sacré nom d'un tonnerre que cela ne se passerait pas ainsi.
Minute! dit-il, je me charge de la chose!...
Il s'éloigna, et un quart d'heure après reparut, traînant par le licol une vieille jument de labour, bien lente, bien lourde, qu'on harnacha tant bien que mal et qu'on attela au cabriolet...
On irait au pas, mais on irait.
À cela ne devait pas se borner la complaisance du vieux troupier.
Sa mission était terminée, puisque Monsieur d'Escorval était arrêté, et il n'avait plus qu'à rejoindre son régiment.
Il déclara donc qu'il ne laisserait pas des dames voyager seules, de nuit, sur une route où elles seraient exposées à de fâcheuses rencontres, et qu'il les escorterait avec ses deux grenadiers...
Et tant pis pour qui s'y frotterait, disait-il en faisant sonner la crosse de son fusil sous sa main nerveuse, pékin ou militaire, on s'en moque! pas vrai, vous autres?
Comme toujours, les deux hommes approuvèrent par un juron.
Et en effet, tout le long de la route, M
me
me
d'Escorval et Marie-Anne les aperçurent précédant ou suivant la voiture, marchant à côté le plus souvent.
Aux portes de Montaignac seulement, le vieux soldat quitta ses protégées, non sans les avoir respectueusement saluées, tant en son nom qu'en celui de ses deux hommes, non sans s'être mis à leur disposition si elles avaient jamais besoin de lui, Bavois, caporal de grenadiers, un
ère
ère
compagnie, caserné à la citadelle...
Dix heures sonnaient, quand M
me
me
d'Escorval et Marie-Anne mirent pied à terre dans la cour de l'
Hôtel de France
.
Elles trouvèrent Maurice désespéré et l'abbé Midon perdant courage.
C'est que, depuis l'instant où Maurice avait écrit, les événements avaient marché, et avec quelle épouvantable rapidité!...
On connaissait maintenant les ordres arrivés par le télégraphe; ils avaient été imprimés et affichés...
Le télégraphe avait dit.
Montaignac doit être regardé comme en état de siège.
Les autorités militaires ont un pouvoir discrétionnaire.
Une commission militaire fonctionnera aux lieu et place de la Cour prévôtale.
Que les citoyens paisibles se rassurent, que les mauvais tremblent!
Quant aux rebelles, le glaive de la loi va les frapper
!...
Six lignes en tout...
mais chaque mot était une menace.
Ce qui surtout faisait frémir l'abbé Midon, c'était la substitution d'une commission à la Cour prévôtale.
Cela renversait tous ses plans, stérilisait toutes ses précautions, enlevait les dernières chances de salut.
La Cour prévôtale était certes expéditive et passionnée, mais du moins elle se piquait d'observer les formes, elle gardait quelque chose encore de la solennité de la justice régulière qui, avant de frapper, veut être éclairée.
Une commission militaire devait infailliblement négliger toute procédure, et juger les accusés sommairement, comme en temps de guerre on juge un espion.
Quoi!...
s'écriait Maurice, on oserait condamner sans enquête, sans audition de témoins, sans confrontation, sans laisser aux accusés le temps de rassembler les éléments de leur défense!...
L'abbé Midon se tut...
Ses plus sinistres prévisions étaient dépassées...
Désormais, il croyait tout possible...
Maurice parlait d'enquête...
Elle avait commencé dans la journée, et elle se poursuivait, en ce moment même, à la lueur des lanternes des geôliers.
C'est-à-dire que le duc de Sairmeuse et le marquis de Courtomieu, relégué au second plan par la mise en état du siège, passaient la revue des prisonniers...
Ils en avaient trois cents, et ils avaient décidé qu'ils choisiraient dans ce nombre, pour les livrer à la commission, les trente plus coupables.
Comment les choisirent-ils, à quoi reconnurent-ils le degré de culpabilité de chacun de ces malheureux?...
Ils eussent été bien embarrassés de le dire.
Ils allaient de l'un à l'autre, posaient quelques questions au hasard, et, d'après ce que l'homme terrifié répondait, selon qu'ils lui trouvaient une bonne ou une mauvaise figure, ils disaient au greffier qui les accompagnait.
Pour demain, celui-là...
ou pour plus tard, cet autre.
Au jour, il y avait trente noms sur une feuille de papier, et les deux premiers étaient ceux du baron d'Escorval et de Chanlouineau.
Aucun des infortunés réunis à l'
Hôtel de France
ne pouvait soupçonner cela, et cependant ils suèrent leur agonie pendant cette nuit, qui leur parut éternelle...
Enfin l'aube fit pâlir la lampe, on entendit battre la diane à la citadelle; l'heure où il était possible de commencer de nouvelles démarches arriva...
L'abbé Midon annonça qu'il allait se rendre seul chez le duc de Sairmeuse, et qu'il saurait bien forcer les consignes...
Il avait baigné d'eau fraîche ses yeux rougis et gonflés, et il se disposait à sortir, quand on frappa discrètement à la porte de la chambre.
Maurice cria.
entrez, et tout aussitôt monsieur Langeron se présenta.
Sa physionomie seule annonçait un grand malheur, et en réalité, le digne homme était consterné.
Il venait d'apprendre que la commission militaire était constituée.
Au mépris de toutes les lois humaines et des règles les plus vulgaires de la justice, la présidence de ce tribunal de vengeance et de haine avait été attribuée au duc de Sairmeuse...
Et il l'avait acceptée, lui que son rôle pendant les événements allait rendre tout à la fois acteur, témoin et juge...
Les autres membres étaient tous militaires.
Et quand la commission entre-t-elle en fonctions? demanda l'abbé Midon...
Aujourd'hui même, répondit l'hôtelier d'une voix hésitante, ce matin...
dans une heure...
peut-être plus tôt!...
L'abbé Midon comprit bien que monsieur Langeron voulait et n'osait dire.
La commission s'assemble, hâtez-vous.
Venez! dit-il à Maurice, je veux être présent quand on interrogera votre père...
Ah! que n'eût pas donné la baronne pour suivre le prêtre et son fils!
Elle ne le pouvait, elle le comprit et se résigna...
Ils partirent donc, et une fois dans la rue, ils aperçurent un soldat qui de loin leur faisait un signe amical.
Ils reconnurent le caporal Bavois et s'arrêtèrent.
Mais, lui, passa près d'eux, de l'air le plus indifférent, comme s'il ne les eût pas connus; seulement, en passant, il leur jeta cette phrase.
J'ai vu Chanlouineau...
bon espoir...
il promet de sauver Monsieur d'Escorval!...
fin du chapitre vingt-six de l'honneur du nom.
Enregistré par Ezwa en Belgique en avril deux mille neuf.