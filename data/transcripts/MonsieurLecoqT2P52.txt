Chapitre LII de l'honneur du nom.
Deuxième partie de Monsieur Lecoq.
Cette enregistrement librivox fait partie du domaine public.
Monsieur Lecoq par Émile Gaboriau.
Deuxième partie l'honneur du nom.
Chapitre LII.
À demi-couchée sur un canapé, le coude sur les coussins, le front dans la main, M
me
me
Blanche écoutait la lecture d'un livre nouveau que lui faisait tante Médie.
L'entrée du domestique ne lui fit seulement pas lever la tête.
Un homme? interrogea-t-elle, quel homme?
Elle n'attendait personne.
Dans sa pensée, celui qui venait ainsi ne pouvait être qu'un des ouvriers employés par Martial.
Je ne puis renseigner madame la marquise, répondit le domestique.
Cet individu est tout jeune, il est vêtu comme les paysans, je supposais qu'il cherchait une place...
C'est sans doute Monsieur le marquis qu'il veut voir?
Madame m'excusera, c'est bien à Madame qu'il veut parler, il me l'a dit.
Alors, sachez comme il s'appelle et ce qu'il désire.
Et se retournant vers la parente pauvre.
Continue, tante, dit M
me
me
Blanche, on nous a interrompues au passage le plus intéressant.
Mais tante Médie n'avait pas eu le temps de finir la page, que déjà le domestique était de retour.
L'homme, dit-il, prétend que madame la marquise comprendra ce dont il s'agit dès qu'elle saura son nom.
Et ce nom?
Chupin.
Ce fut comme un obus éclatant tout à coup dans le salon de l'hôtel Meurice.
Tante Médie eut un gémissement étouffé; elle laissa son livre et s'affaissa sur sa chaise, tout inerte, les bras pendants.
M
me
me
Blanche, elle, se dressa tout d'une pièce, plus pâle que son peignoir de cachemire blanc, l'œil trouble, les lèvres tremblantes.
Chupin! répétait-elle, comme si elle eût espéré qu'on allait lui dire qu'elle avait mal entendu, Chupin!...
Puis, avec une certaine violence.
Répondez à cet homme que je ne veux ni le voir ni l'entendre.
Il est inutile qu'il se représente.
Jamais je ne le recevrai!...
Mais, dans le temps que mit le domestique à s'incliner respectueusement et à gagner la porte à reculons, la jeune femme se ravisa.
Au fait, non, prononça-t-elle, j'ai réfléchi, faites monter cet homme.
Oui, approuva tante Médie d'une voix défaillante, qu'il vienne, cela vaut mieux.
Le domestique sortit, et les deux femmes restèrent en face l'une de l'autre, immobiles, consternées, le cœur serré par les plus effroyables appréhensions, la gorge serrée au point de ne pouvoir qu'à grand peine articuler quelques paroles.
C'est un des fils de ce vieux scélérat de Chupin, dit enfin M
me
me
Blanche.
En effet, je le crois, mais que veut-il?
Quelque secours, probablement.
La parente pauvre leva les bras au ciel.
Fasse Dieu qu'il ignore tes rendez-vous avec son père, Blanche, prononça-t-elle.
Doux Jésus!...
pourvu qu'il ne sache rien!
Eh! que veux-tu qu'il sache.
Ne vas-tu pas te désespérer à l'avance!
Dans dix minutes, nous serons fixées.
D'ici là, tante, du calme.
Et même, crois-moi, tourne-nous le dos, regarde dans la rue pour qu'on ne voie pas ta figure...
Mais pourquoi ce coquin tarde-t-il tant à paraître...
M
me
me
Blanche ne se trompait pas.
C'était bien l'aîné des Chupin qui était là, celui à qui le vieux maraudeur mourant avait confié son secret.
Depuis son arrivée à Paris, il battait le pavé du matin au soir, demandant partout et à tous l'adresse du marquis de Sairmeuse.
On venait de lui indiquer l'hôtel Meurice, et il accourait.
Ce n'est toutefois qu'après s'être bien assuré de l'absence de Martial qu'il avait demandé M
me
me
la marquise.
Il attendait le résultat de sa démarche sous le porche, debout, les mains dans les poches de sa veste, sifflotant, lorsque le domestique revint en lui disant.
On consent à vous recevoir, suivez-moi.
Chupin suivit; mais le domestique, extraordinairement intrigué et tout brûlant de curiosité, ne se hâtait pas, espérant tirer quelque éclaircissement de ce campagnard.
Ce n'est pas pour vous flatter, mon garçon, dit-il, mais votre nom a produit un fier effet sur M
me
me
la marquise!
Le prudent paysan dissimula sous un sourire niais la joie dont l'inonda cette nouvelle.
Comme ça, poursuivit le domestique, elle vous connaît?
Un petit peu.
Vous êtes pays?
Je suis son frère de lait.
Le domestique n'en crut pas un mot; il soupçonnait bien autre chose, vraiment!
Cependant, comme il était arrivé à la porte de l'appartement du marquis de Sairmeuse, il ouvrit et poussa Chupin dans le salon.
Le mauvais gars avait d'avance préparé une petite histoire, mais il fut si bien ébloui de la magnificence du salon, qu'il resta court et béant.
Ce qui l'interloquait surtout, c'était une grande glace, en face de la porte, où il se voyait en pied, et les belles fleurs du tapis qu'il craignait d'écraser sous ses gros souliers.
Après un moment, voyant qu'il demeurait stupide, un sourire idiot sur les lèvres, tortillant son chapeau de feutre, M
me
me
Blanche se décida à rompre le silence.
Vous désirez?...
demanda-t-elle.
Le gars Chupin était intimidé, mais il n'avait point peur.
ce n'est pas du tout la même chose.
Il garda son masque de gaucherie, mais recouvrant son aplomb, il se mit à débiter avec, un accent traînard toutes les formules de respect qu'il savait.
Au fait, insista la jeune femme impatientée.
Amener au fait un paysan n'est pas facile, et ce n'est qu'après beaucoup de vaines paroles encore, que Chupin expliqua longuement qu'il avait été obligé de quitter le pays à cause des ennemis qu'il y avait, qu'on n'avait pas retrouvé le trésor de son père, qu'il était, en conséquence, sans ressources...
Oh! assez! interrompit M
me
me
Blanche.
Puis, d'un ton qui n'était rien moins que bienveillant.
Je ne vois pas, continua-t-elle, à quel titre vous vous adressez à moi.
Vous aviez, comme toute votre famille, une réputation détestable à Sairmeuse.
Enfin, n'importe, vous êtes de mon pays, je consens à vous accorder un secours, à la condition que vous n'y reviendrez pas.
C'est d'un air moitié humble et moitié goguenard que Chupin écouta cette semonce.
À la fin, il releva la tête.
Je ne demande pas l'aumône, articula-t-il fièrement.
Que demandez-vous donc?
Mon dû.
M
me
me
Blanche reçut un coup dans le cœur, et cependant, elle eut le courage de toiser Chupin d'un air dédaigneux, en disant.
Ah! je vous dois quelque chose!...
Pas à moi personnellement, madame la marquise, mais à mon défunt père.
Au service de qui donc a-t-il péri?
Pauvre vieux!
Il vous aimait bien, allez...
tout comme moi, du reste.
Sa dernière parole, avant de mourir, a été pour vous.
Vois-tu, gars, qu'il me dit, il vient de se passer des choses terribles à la Borderie.
La jeune dame de Monsieur le marquis en voulait à Marie-Anne, et elle lui a fait passer le goût du pain.
Sans moi, elle était perdue.
Quand je serai crevé, laisse-moi tout mettre sur le dos, la terre n'en sera pas plus froide et ça innocentera la jeune dame...
Et après, elle te récompensera bien, et tant que tu te tairas tu ne manqueras de rien...
Si grande que fût son impudence, il s'arrêta, stupéfait de la physionomie de M
me
me
Blanche.
En présence de cette dissimulation supérieure, il douta presque du récit de son père.
C'est que véritablement la jeune femme fut héroïque en ce moment.
Elle avait compris que céder une fois c'était se mettre à la discrétion de ce misérable, comme elle était déjà à la merci de tante Médie.
Et avec une merveilleuse énergie, elle payait d'audace.
En d'autres termes, fit-elle, vous m'accusez du meurtre de M
lle
lle
Lacheneur, et vous me menacez de me dénoncer si je ne vous accorde pas ce que vous allez exiger?
Le gars Chupin inclina affirmativement la tête.
Eh bien!...
reprit M
me
me
Blanche, puisqu'il en est ainsi, sortez!...
Il est sûr qu'elle allait, à force d'audace, gagner cette partie périlleuse, dont le repos de sa vie était l'enjeu; Chupin était absolument déconcerté, lorsque tante Médie qui écoutait, debout devant la fenêtre, se retourna, tout effarée, en criant.
Blanche!...
ton mari...
Martial!...
Il entre...
il monte.
La partie fut perdue...
La jeune femme vit son mari arrivant, trouvant Chupin, le faisant parler, découvrant tout.
Sa tête s'égara, elle s'abandonna, elle se livra.
Brusquement elle mit sa bourse dans la main du misérable et l'entraîna, par une porte intérieure, jusqu'à l'escalier de service.
Prenez toujours cela, disait-elle d'une voix sourde, ce n'est qu'un à-compte...
Nous nous reverrons.
Et pas un mot!
Pas un mot à mon mari, surtout!...
Elle avait été bien inspirée de ne pas perdre une minute; lorsqu'elle rentra, elle trouva Martial dans le salon.
Il était assis, la tête inclinée sur la poitrine, et tenait à la main une lettre déployée.
Au bruit que fit sa femme, il se dressa, et elle put voir rouler dans ses yeux une larme furtive.
Quel malheur nous frappe encore!...
balbutia-t-elle d'une voix que l'excès de son émotion de tout à l'heure rendait à peine intelligible.
Martial ne remarqua pas ce mot encore, qui l'eût au moins étonné.
Mon père est mort, Blanche, prononça-t-il.
Le duc de Sairmeuse!...
Mon Dieu!...
Comment cela?...
D'une chute de cheval, dans les bois de Courtomieu, près des roches de Sanguille...
Ah!...
c'est là que mon pauvre père a failli être assassiné.
Oui...
c'est au même endroit, en effet.
Un moment de silence suivit.
Martial n'aimait que très-médiocrement son père, et il n'en était pas aimé, il le savait; et il s'étonnait de l'amère tristesse qui l'envahissait en songeant qu'il n'était plus.
Puis, il y avait autre chose encore.
D'après cette lettre, que m'apporte un exprès, poursuivit-il, tout le monde, à Sairmeuse, croit à un accident.
Mais moi!...
moi!...
Eh bien!...
Moi, je crois à un crime.
Une exclamation d'effroi échappa à tante Médie, et M
me
me
Blanche pâlit.
À un crime!...
murmura-t-elle.
Oui, Blanche, et je pourrais nommer le coupable.
Oh! mes pressentiments ne me trompent pas.
Le meurtrier de mon père est celui qui a tenté d'assassiner le marquis de Courtomieu...
Jean Lacheneur!...
Martial baissa tristement la tête.
C'était répondre.
Et vous ne le dénoncez pas, s'écria la jeune femme, et vous ne courez pas demander vengeance à la justice!...
La physionomie de Martial devenait de plus en plus sombre.
À quoi bon!...
répondit-il.
Je n'ai à donner que des preuves morales, et c'est des preuves matérielles qu'il faut à la justice.
Il eut un geste d'affreux découragement, et, d'une voix sourde, répondant à ses pensées plutôt que s'adressant à sa femme, il poursuivit.
Le duc de Sairmeuse et le marquis de Courtomieu ont récolté ce qu'ils avaient semé.
La terre ne boit jamais le sang répandu, et tôt ou tard le crime s'expie.
M
me
me
Blanche frémissait.
Chacune des paroles de son mari trouvait un écho en elle.
Il eût parlé pour elle qu'il ne se fût pas exprimé autrement.
Martial, fit-elle, essayant de le détourner de ses funèbres préoccupations, Martial!
Il ne parut pas l'entendre, et du même ton il continua.
Ces Lacheneur vivaient heureux et honorés avant notre arrivée à Sairmeuse.
Leur conduite a été au-dessus de tout éloge, ils ont poussé la probité jusqu'à l'héroïsme.
D'un mot, nous pouvions nous les attacher et en faire nos amis les plus sûrs et les plus dévoués...
C'était notre devoir avant notre intérêt.
Nous ne l'avons pas compris.
Nous les avons humiliés, ruinés, exaspérés, poussés à bout...
De telles fautes se payent.
Il est de ces gens qu'on doit respecter, si on n'est pas sûr de les anéantir d'un coup, eux et les leurs...
Qui me dit qu'à la place de Jean Lacheneur, je n'agirais pas comme lui.
Il se tut un moment, puis, éclairé par un de ces rapides et éblouissants éclairs, qui parfois déchirent les ténèbres de l'avenir.
Seul je connais bien Jean Lacheneur, reprit-il; seul j'ai pu mesurer sa haine, et je sais qu'il ne vit plus que par l'espoir de se venger de nous...
Certes nous sommes bien haut et il est bien bas, n'importe!
Nous avons tout à craindre.
Nos millions sont comme un rempart autour de nous, c'est vrai, mais il saura s'ouvrir une brèche.
Et les plus minutieuses précautions ne nous sauveront pas.
un moment viendra quand même où nos défiances s'assoupiront, tandis que sa haine veillera toujours.
Qu'entreprendra-t-il, je n'en sais rien, mais ce sera terrible.
Souvenez-vous de mes paroles, Blanche, si le malheur entre dans notre maison, c'est que Jean Lacheneur lui aura ouvert la porte...
Tante Médie et sa nièce étaient trop bouleversées pour articuler seulement une parole, et pendant cinq minutes on n'entendit que le pas de Martial qui arpentait le salon.
Enfin il s'arrêta devant sa femme.
Je viens d'envoyer chercher des chevaux de poste, dit-il...
Vous m'excuserez de vous laisser seule ici...
Il faut que je me rende à Sairmeuse...
Je ne serai pas absent plus d'une semaine.
Il partit, en effet, quelques heures plus tard, et M
me
me
Blanche se trouva abandonnée à elle-même et maîtresse d'elle pour plusieurs jours.
Ses angoisses étaient plus intolérables encore qu'au lendemain du crime.
Ce n'était plus contre des fantômes qu'elle avait à se défendre maintenant; Chupin existait, et sa voix, si elle n'était pas plus terrible que celle de la conscience, pouvait être entendue.
Si M
me
me
Blanche eût su où le prendre, le misérable, elle eût traité avec lui.
Elle eût obtenu, pensait-elle, moyennant une grosse somme, qu'il quittât Paris, la France, qu'il s'en allât si loin qu'on n'entendit plus jamais parler de lui...
Naturellement Chupin était sorti de l'hôtel sans rien dire...
Les sinistres pressentiments exprimés par Martial, ajoutaient encore à l'épouvante de la jeune femme.
Elle aussi, rien qu'au nom de Lacheneur, se sentait remuée jusqu'au plus profond de ses entrailles.
Elle ne pouvait s'ôter l'idée qu'il soupçonnait quelque chose, et que, des bas fonds de la société où le retenait sa misère, il la guettait...
C'est alors que plus vivement que jamais elle désira retrouver l'enfant de Marie-Anne.
Outre qu'elle se débarrasserait ainsi des obsessions de son serment violé, il lui semblait que cet enfant la protégerait peut-être un jour et qu'il serait entre ses mains comme un otage.
Mais où rencontrer un homme à qui se confier?...
Se mettant l'esprit à la torture, elle se souvint d'avoir entendu autrefois son père parler d'un espion du nom de Chefteux, garçon prodigieusement adroit, disait-il, et capable de tout, même d'honnêteté, quand on y mettait le prix.
C'était un de ces misérables comme il en grouille dans les bourbiers de la politique, aux époques troublées, un jeune mouchard dressé par Fouché, qui avait toute honte bue, qui avait servi et trahi tour à tour tous les partis, qui avait trafiqué de tout, et qui, en dernier lieu, avait été condamné pour faux et s'était évadé du bagne.
En mille huit cent quinze, Chefteux avait quitté ostensiblement la police, pour fonder un bureau de renseignements privés.
Après quelques informations, M
me
me
Blanche apprit que cet homme demeurait place Dauphine, et elle résolut de profiter de l'absence de son mari pour s'adresser à lui.
Un matin donc, elle s'habilla le plus simplement possible et, suivie de tante Médie, elle alla frapper à la porte de l'élève de Fouché.
Chefteux avait alors trente-quatre ans.
C'était un petit homme de taille moyenne, de mine inoffensive, et qui affectait une continuelle bonne humeur.
Il fit entrer ses deux clientes dans un petit salon fort proprement meublé, et tout aussitôt M
me
me
Blanche se mit à lui raconter qu'elle était mariée et établie rue Saint-Denis, et qu'une de ses sœurs, qui venait de mourir, avait fait une faute, et qu'elle était prête aux plus grands sacrifices pour retrouver l'enfant de cette sœur, etc., etc., enfin, tout une histoire, qu'elle avait préparée, et qui était assez vraisemblable.
L'espion n'en crut pourtant pas un mot, car, dès qu'elle eut achevé, il lui frappa familièrement sur l'épaule, en disant.
Bref, la petite mère, nous avons fait nos farces avant le mariage...
Elle se rejeta en arrière, comme au contact d'un reptile, écrasant du regard l'homme des renseignements.
Être traitée ainsi, elle, une Courtomieu, duchesse de Sairmeuse!
Je crois que vous vous méprenez! fit-elle d'un accent où vibrait tout l'orgueil de sa race.
Il se le tint pour dit, et se confondit en excuses.
Mais tout en écoutant et en notant les indispensables détails que lui donnait la jeune femme, il pensait.
Quel œil! quel ton!...
De la part d'une bourgeoise du quartier Saint-Denis, c'est louche...
Ses soupçons furent confirmés par la somme de vingt francs que lui promit imprudemment M
me
me
Blanche en cas de succès et par la consignation de cinq cents francs d'arrhes.
Et où aurai-je l'honneur de vous adresser mes communications, madame?...
demanda-t-il.
Nulle part...
répondit la jeune femme, je passerai ici de temps à autre...
Lorsqu'il reconduisit ses clientes, l'espion ne doutait plus...
Dès qu'il les jugea au bas de l'escalier, il s'élança dehors en se disant.
Pour le coup, je crois que la chance me sourit.
Suivre ces deux clientes que lui envoyait sa bonne étoile, s'informer, découvrir leur nom et leur qualité n'était qu'un jeu pour l'ancien agent de Fouché.
Il avait la partie d'autant plus belle, qu'elles étaient à mille lieues de soupçonner ses desseins.
La bassesse du personnage et sa générosité, à elle, rassuraient absolument M
me
me
Blanche.
Il lui avait d'ailleurs si fort vanté ses prodigieux moyens d'investigations, qu'elle se tenait pour certaine du succès.
Tout en regagnant l'hôtel Meurice, elle s'applaudissait de sa démarche.
Avant un mois, disait-elle à tante Médie, nous aurons cet enfant; je le ferai élever secrètement et il sera notre sauvegarde...
La semaine suivante, seulement, elle reconnut l'énormité de son imprudence.
Étant retournée chez Chefteux, il l'accueillit avec de telles marques de respect, qu'elle vit bien qu'elle était connue...
Consternée, elle essaya de donner le change, mais l'espion l'interrompit.
Avant tout, fit-il avec un bon sourire, je constate l'identité des personnes qui m'honorent de leur confiance.
C'est comme un échantillon de mon savoir-faire, que je donne...
gratis.
Mais que madame la duchesse soit sans crainte.
je suis discret par caractère et par profession.
Nous avons d'ailleurs quantité de dames de la plus haute volée dans la position de madame la duchesse.
Un petit accident avant le mariage est si vite arrivé!...
Ainsi Chefteux était persuadé que c'était son enfant à elle, que la jeune duchesse de Sairmeuse faisait rechercher.
Elle n'essaya pas de le dissuader.
Mieux valait qu'il crût cela que s'il eût soupçonné la vérité.
M
me
me
Blanche rentra dans un état à faire pitié.
Elle se sentait comme prise sous un inextricable filet, et à chaque mouvement, loin de se dégager, elle resserrait les mailles.
Le secret de sa vie et de son honneur, trois personnes le possédaient.
Comment dans de telles conditions espérer garder un secret, cette chose subtile qui, le temps seulement de passer de la bouche à une oreille amie, s'évapore et se répand!
Elle se voyait trois maîtres qui d'un geste, d'un mot, d'un regard, pouvaient plier sa volonté comme une baguette de saule.
Et elle n'était plus libre comme autrefois.
Martial était revenu.
Le temps avait marché.
La somptueuse installation de l'hôtel de Sairmeuse était terminée...
Désormais, la jeune duchesse était condamnée à vivre sous les yeux de cinquante domestiques, de quarante ennemis au moins, par conséquent intéressés à la surveiller, à épier ses démarches, à deviner jusqu'à ses plus intimes pensées.
Il est vrai que tante Médie lui était plus utile que nuisible.
Elle lui achetait une robe toutes les fois qu'elle s'en achetait une, elle la traînait partout à sa suite, et la parente pauvre se déclarait ravie et prête à tout.
Chefteux n'inquiétait pas non plus beaucoup M
me
me
Blanche.
Tous les trois mois, il présentait un mémoire de frais d'investigations s'élevant à dix mille francs environ, et il était clair que tant qu'on le payerait il se tairait.
L'ancien espion n'avait d'ailleurs pas fait mystère de l'espoir qu'il avait d'une rente viagère de vingt-quatre mille francs.
M
me
me
Blanche lui ayant dit, après deux années, qu'il devait renoncer à ses explorations puisqu'il n'aboutissait à rien.
Jamais, répondit-il, je chercherai tant que je vivrai...
à tout prix.
Restait Chupin malheureusement...
Pour commencer, il avait fallu lui compter vingt mille francs, d'un seul coup...
Son frère cadet venait de le rejoindre, l'accusant d'avoir volé le magot paternel, et réclamant sa part un couteau à la main.
Il y avait eu bataille, et c'est la tête tout enveloppée de linges ensanglantés que Chupin s'était présenté à M
me
me
Blanche.
Donnez-moi, lui avait-il dit, la somme que le vieux avait enterrée, et je laisserai croire à mon frère que je l'avais prise...
C'est bien désagréable de passer pour un voleur, quand on est honnête, mais je supporterai cela pour vous...
Si vous refusez, par exemple, il faudra bien que je lui avoue d'où je tire mon argent, et comment...
S'il avait toutes les corruptions, les vices et la froide perversité du vieux maraudeur, ce misérable n'en avait ni l'intelligence ni la finesse.
Loin de s'entourer de précautions, comme le lui commandait son intérêt, il semblait prendre, à compromettre la duchesse, un plaisir de brute.
Il assiégeait l'hôtel de Sairmeuse.
On ne voyait que lui pendu à la cloche.
Et il venait à toute heure, le matin, l'après-midi, le soir, sans s'inquiéter de Martial.
Et les domestiques étaient stupéfaits de voir que leur maîtresse, si hautaine, quittait tout, sans hésiter, pour cet homme de mauvaise mine, qui empestait le tabac et l'eau-de-vie.
Une nuit qu'il y avait une grande fête à l'hôtel de Sairmeuse, il se présenta ivre, et impérieusement exigea qu'on allât prévenir M
me
me
Blanche qu'il était là et qu'il attendait.
Elle accourut avec sa magnifique toilette décolletée, blême de rage et de honte sous son diadème de diamants...
Et comme, dans son exaspération, elle refusait au misérable ce qu'il demandait.
C'est-à-dire que je crèverais de faim pendant que vous faites la noce!...
s'écria-t-il.
Pas si bête!
De la monnaie, et vite, ou je crie tout ce que je sais!
Que faire? céder.
La duchesse s'exécuta, comme toujours.
Et cependant, il devenait de jour en jour plus insatiable.
L'argent ne tenait pas plus dans ses poches que l'eau dans un crible.
Qu'en faisait-il?...
Sans doute, il l'éparpillait sans en comprendre la valeur, il le gaspillait insoucieusement et stupidement, comme le voleur qui a fait un beau coup, que l'or grise, et qui d'ailleurs se croit riche de tout ce qu'il y a à voler au monde.
Lui faisait un beau coup tous les jours...
N'importe! c'était à n'y rien comprendre, car il n'avait même pas eu l'idée de hausser ses vices aux proportions de la fortune qu'il prodiguait.
Il ne songeait même pas à se vêtir proprement, il semblait à la mendicité.
Il restait fidèle à la boue et à la plus basse crapule.
Peut-être ne se soûlait-il à l'aise que dans un bouge ignoble.
Il lui fallait pour compagnons les plus dégoûtants gredins, les plus abjects et les plus vils.
C'est à ce point qu'une nuit il fut arrêté dans un endroit immonde.
La police, émue de voir tant d'or entre les mains d'un tel misérable, crut à un crime.
Il nomma la duchesse de Sairmeuse.
Martial était à Vienne à ce moment, par bonheur, car le lendemain un inspecteur de la Préfecture se présenta à l'hôtel...
Et M
me
me
Blanche subit cette atroce humiliation de confesser que c'était elle, en effet, qui avait remis une grosse somme à cet homme, dont elle avait connu la famille, ajoutait-elle, et qui lui avait rendu des services autrefois...
Souvent le misérable avait des lubies.
Il déclarait, par exemple, que se présenter sans cesse à l'hôtel de Sairmeuse lui répugnait, que les domestiques le traitaient comme un mendiant et que cela l'humiliait; bref, qu'il écrirait désormais...
Et le lendemain, en effet, il écrivait à M
me
me
Blanche.
Apportez-moi telle somme, à telle heure, à tel endroit.
Et elle, la fière duchesse de Sairmeuse, elle était toujours exacte au rendez-vous.
Puis, c'était sans cesse quelque invention nouvelle, comme s'il eût trouvé une jouissance extraordinaire à constater continuellement son pouvoir et à en abuser.
C'était à le croire, tant il y déployait de science, de méchanceté et de raffinements cruels.
Il avait rencontré, Dieu sait où une certaine Aspasie Clapard, il s'en était épris, et bien qu'elle fût plus vieille que lui, il avait voulu l'épouser.
M
me
me
Blanche avait payé la noce...
Une autre fois, il voulut s'établir, résolu, disait-il, à vivre de son travail.
Il acheta un fonds de marchand de vin que la duchesse paya et qui fut bu en un rien de temps.
Il eut un enfant, et M
me
me
de Sairmeuse dut payer le baptême comme elle avait payé la noce, trop heureuse que Chupin n'exigeât pas qu'elle fût marraine du petit Polyte.
Il avait eu un moment cette idée...
À deux reprises, M
me
me
Blanche fut obligée d'accompagner à Vienne et à Londres, son mari, chargé d'importantes missions diplomatiques.
Elle resta près de trois ans à l'étranger...
Eh bien! pendant tout ce temps, elle reçut chaque semaine une lettre, au moins, de Chupin...
Ah! que de fois elle envia le sort de sa victime!
Qu'était, comparée à sa vie, la mort de Marie-Anne!...
Elle souffrait depuis autant d'années bientôt que Marie-Anne avait souffert de minutes, et elle se disait que les tortures du poison ne devaient pas être bien plus intolérables que ses angoisses...
fin du chapitre LII de l'honneur du nom.
Enregistré par Ezwa en Belgique en mai deux mille dix.