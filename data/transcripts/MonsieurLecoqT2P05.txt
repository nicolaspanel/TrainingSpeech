Chapitre cinq de l'honneur du nom.
Deuxième partie de Monsieur Lecoq.
Cette enregistrement librivox fait partie du domaine public.
Monsieur Lecoq par Émile Gaboriau.
Deuxième partie l'honneur du nom.
Chapitre cinq.
L'habitation du baron d'Escorval, cette construction de briques à saillies de pierres blanches, qu'on apercevait de l'avenue superbe de Sairmeuse, était petite et modeste.
Son seul luxe était un joli parterre dont les gazons se déroulaient jusqu'à l'Oiselle, et un parc assez vaste délicieusement ombragé.
Dans le pays on disait.
le château d'Escorval, mais c'était pure flatterie.
Le moindre manufacturier enrichi d'un coup de hausse eût voulu mieux, plus grand, plus beau, plus brillant et plus voyant surtout.
C'est que Monsieur d'Escorval—et ce lui sera dans l'histoire un éternel honneur—n'était pas riche.
Après avoir été chargé de nombre de ces missions d'où généraux et administrateurs revenaient lourds de millions à crever les chevaux de poste le long de la route, Monsieur d'Escorval restait avec le seul patrimoine que lui avait légué son père.
vingt à vingt-cinq mille livres de rentes au plus.
Cette simple maison, à trois quarts de lieues de Sairmeuse, représentait ses économies de dix années.
Lui-même l'avait fait bâtir vers mille huit cent six, sur un plan tracé de sa main, et elle était devenue son séjour de prédilection.
Il se hâtait d'y accourir dès que ses travaux lui laissaient quelques journées, heureux de la solitude et des ombrages de son parc.
Mais cette fois il n'était pas venu à Escorval de son plein gré.
Il venait d'y être exilé par la liste de mort et de proscription du vingt-quatre juillet, cette même liste fatale qui envoyait devant un conseil de guerre l'enthousiaste Labédoyère et l'intègre et vertueux Drouot.
Cependant, en cette solitude même des campagnes de Montaignac, sa situation n'était pas exempte de périls.
Il était de ceux qui, quelques jours avant le désastre de Waterloo, avaient le plus vivement pressé l'Empereur de faire fusiller Fouché, l'ancien ministre de la police.
Or, Fouché savait ce conseil et il était tout-puissant.
Gardez-vous!...
écrivaient à Monsieur d'Escorval ses amis de Paris.
Lui s'en remettait à la Providence, envisageant l'avenir, si menaçant qu'il dût paraître, avec l'inaltérable sérénité d'une conscience pure.
Le baron d'Escorval était un homme jeune encore, il n'avait pas cinquante ans; mais les soucis, les travaux, les nuits passées aux prises avec les difficultés les plus ardues de la politique impériale l'avaient vieilli avant l'âge.
Il était grand, légèrement chargé d'embonpoint et un peu voûté.
Ses yeux calmes malgré tout, sa bouche sérieuse, son large front dépouillé, ses manières austères inspiraient le respect.
Il doit être dur et inflexible, disaient ceux qui le voyaient pour la première fois.
Ils se trompaient.
Si, dans l'exercice de ses fonctions, ce grand homme ignoré sut résister à tous les entraînements et aux plus furieuses passions, s'il restait de fer dès qu'il s'agissait du devoir, il redevenait dans la vie privée simple comme l'enfant, doux et bon jusqu'à la faiblesse.
À ce beau caractère, noblement apprécié, il dut la félicité de sa vie.
Il lui dut ce bonheur du ménage, que n'envie pas le vulgaire qui l'ignore, bonheur rare et précieux, si pénétrant et si doux, qui emplit la vie et l'embaume comme un céleste parfum.
À l'époque la plus sanglante de la Terreur, Monsieur d'Escorval avait arraché au bourreau une jeune ci-devant, Victoire-Laure de l'Alleu, arrière-cousine des Rhéteau de Commarin, belle comme un ange et moins âgée que lui de trois ans seulement.
Il l'aima...
et bien qu'elle fût orpheline et qu'elle n'eût rien, il l'épousa, estimant que les trésors de son cœur vierge valaient la dot la plus magnifique.
Celle-là fut une honnête femme, comme son mari était un honnête homme, dans le sens strict et rigoureux du mot.
On la vit peu aux Tuileries, dont le rang de Monsieur d'Escorval lui ouvrit les portes.
Les splendeurs de la cour impériale, qui dépassaient alors les pompes de Louis quatorze, n'avaient pas d'attraits pour elle.
Grâces, beauté, jeunesse, elle réservait pour l'intimité du foyer les qualités exquises de son esprit et de son cœur.
Son mari fut son Dieu, elle vécut en lui et par lui, et jamais elle n'eut une pensée qui ne lui appartint.
Les quelques heures qu'il dérobait pour elle à ses labeurs opiniâtres étaient ses heures de fête.
Et lorsque le soir, à la veillée, ils étaient assis chacun d'un côté de la cheminée de leur modeste salon, avec leur fils Maurice, jouant entre eux, sur le tapis, il leur paraissait qu'ils n'avaient rien à souhaiter ici-bas.
Les événements de la fin de l'Empire les surprirent en plein bonheur.
Les surprirent...
non.
Il y avait longtemps déjà que Monsieur d'Escorval sentait chanceler le prodigieux édifice du génie dont il avait fait son idole.
Certes, il ressentit un cruel chagrin de la chute, mais il fut navré surtout de l'indigne spectacle des trahisons et des lâchetés qui la suivirent.
Il fut épouvanté et écœuré, quand il vit la levée en masse de toutes les cupidités se précipitant à la curée.
Dans ces dispositions, l'isolement de l'exil devait lui paraître un bienfait...
Sans compter, disait-il à la baronne, que nous serons vite oubliés ici.
Ce n'était pas tout à fait ce qu'il pensait.
Mais, de son côté, sa noble femme gardait un visage tranquille alors qu'elle tremblait pour la sécurité des siens.
Ce premier dimanche d'août, cependant, Monsieur d'Escorval et sa femme étaient plus tristes que de coutume.
Le même pressentiment vague d'un malheur terrible et prochain leur serrait le cœur.
À l'heure même où Lacheneur se présentait chez l'abbé Midon, ils étaient accoudés à la terrasse de leur maison, et ils exploraient d'un œil inquiet les deux routes qui conduisent d'Escorval au château et au village du Sairmeuse.
Prévenu, le matin même, par ses amis de Montaignac de l'arrivée du duc, le baron avait envoyé son fils avertir monsieur Lacheneur.
Il lui avait recommandé d'être le moins longtemps possible...
et malgré cela, les heures s'écoulaient et Maurice ne reparaissait pas.
Pourvu, pensaient-ils chacun à part soi, qu'il ne lui soit rien arrivé!...
Non, il ne lui était rien arrivé...
Seulement un mot de M
lle
lle
Lacheneur avait suffi pour lui faire oublier sa déférence accoutumée aux volontés paternelles.
Ce soir, lui avait-elle dit, je connaîtrai vraiment votre cœur!...
Qu'est-ce que cela signifiait?...
Doutait-elle donc de lui?...
Torturé par les plus douloureuses anxiétés, le pauvre garçon n'avait pu se résoudre à s'éloigner sans une explication, et il avait rôdé autour du château de Sairmeuse, espérant que Marie-Anne reparaîtrait.
Elle reparut, en effet, mais au bras de son père.
Le jeune d'Escorval les suivit de loin, et bientôt il les vit entrer au presbytère.
Qu'y allaient-ils faire?
Il savait que le duc et son fils s'y trouvaient.
Le temps qu'ils y restèrent, et qu'il attendit sur la place lui parut plus long qu'un siècle.
Ils sortirent, cependant, et il s'avançait pour les aborder, quand il fut prévenu par Martial dont il entendit les promesses.
Maurice ne connaissait rien de la vie, son innocence était, autant dire, celle d'un enfant, mais il ne pouvait se méprendre aux intentions qui dictaient la démarche du marquis de Sairmeuse.
À cette pensée que le caprice d'un libertin osait s'arrêter sur cette jeune fille si belle et si pure, qu'il aimait de toutes les forces de son âme, dont il avait juré qu'il ferait sa femme, tout son sang afflua à son cerveau.
Il se dit qu'il se devait de châtier l'insolent, le misérable...
Heureusement—malheureusement peut-être—son bras fut arrêté par le souvenir d'une phrase qu'il avait entendu mille fois répéter à son père.
Le calme et l'ironie sont les seules armes dignes des forts.
Et il eut assez de volonté pour paraître de sang-froid, quand, en réalité, il était hors de lui.
Ce fut Martial qui s'emporta et qui menaça...
Ah! oui...
je te retrouverai, fat!...
répétait Maurice, les dents serrées, en suivant de l'œil son ennemi qui s'éloignait.
Il se retourna alors, mais Marie-Anne et son père l'avaient abandonné, et il les aperçut à plus de cent pas.
Bien que cette indifférence le confondit, il s'empressa de les rejoindre, et adressa la parole à monsieur Lacheneur.
Nous allons chez votre père, lui fut-il répondu d'un ton farouche.
Un regard de son amie lui commandait le silence, il se tut et se mit à marcher à quelques pas en arrière, la tête inclinée sur la poitrine, mortellement inquiet et cherchant vainement à s'expliquer ce qui se passait.
Son attitude trahissait une si réelle douleur, que sa mère la devina, lorsqu'enfin, du haut de la terrasse, elle l'aperçut au tournant du chemin.
Toutes les angoisses que la courageuse femme dissimulait depuis un mois se résumèrent en un cri.
Ah!...
voici le malheur!...
dit-elle...
nous n'y échapperons pas.
C'était le malheur, on n'en pouvait douter à la seule vue de monsieur Lacheneur lorsqu'il entra dans le salon d'Escorval.
Il s'avançait du pas lourd d'un ivrogne, l'œil morne et sans expression, la face injectée, les lèvres blanches et tremblantes.
Qu'y a-t-il!...
demanda vivement le baron...
Mais l'autre ne sembla pas l'entendre.
Ah!...
je l'avais bien prévu, murmura-t-il, continuant un monologue commencé dehors, je l'avais bien dit à ma fille...
M
me
me
d'Escorval, après avoir embrassé Marie-Anne, l'avait attirée près d'elle.
Que se passe-t-il, mon Dieu! interrogeait-elle.
D'un geste empreint de la plus désolante résignation, la jeune fille lui lit signe de regarder et d'écouter son père.
Monsieur Lacheneur paraissait sortir de cet horrible anéantissement,—bienfait de Dieu,—qui suit les crises trop cruelles pour les forces humaines.
Pareil au dormeur que reprennent au réveil les misères oubliées pendant le sommeil, il retrouvait avec la faculté de se souvenir la faculté de souffrir.
Ce qu'il y a, monsieur le baron, répondit-il d'une voix rauque, il y a que je me suis levé ce matin le plus riche propriétaire du pays, et que je me coucherai ce soir plus pauvre que le dernier mendiant de la commune.
J'avais tout, je n'ai plus rien...
rien que mes deux bras.
Ils m'ont gagné mon pain jusqu'à vingt-cinq ans, ils me le gagneront jusqu'à la mort...
J'ai fait un beau rêve, il vient de finir...
Devant l'explosion de ce désespoir, Monsieur d'Escorval pâlissait.
Vous devez vous exagérer votre malheur, balbutia-t-il, expliquez-moi ce qui vous arrive...
Sans avoir certes conscience de ce qu'il faisait, monsieur Lacheneur lança son chapeau sur un fauteuil, et rejeta en arrière ses cheveux gris qu'il portait fort longs.
À vous, je dirai tout, monsieur le baron, reprit-il.
Je suis venu pour cela.
On vous connaît, vous, on connaît votre cœur...
D'ailleurs, ne m'avez-vous pas fait quelquefois l'honneur de m'appeler votre ami?...
Aussitôt, avec la précision brutale de la vérité palpitante, il retraça la scène du presbytère.
Le baron écoutait pétrifié d'étonnement, doutant presque du témoignage de ses sens.
Les exclamations sourdes de M
me
me
d'Escorval disaient à quel point, en elle, tous les nobles sentiments étaient révoltés.
Mais il était un auditeur—Marie-Anne seule l'observait,—que le récit remuait jusqu'au plus profond de ses entrailles.
Cet auditeur était Maurice.
Adossé à la porte, pâle comme la mort, il faisait pour retenir des larmes de douleur et de rage les plus énergiques et aussi les plus inutiles efforts.
Insulter Lacheneur, c'était insulter Marie-Anne, c'est-à-dire l'atteindre, le frapper, l'outrager, lui, dans tout ce qu'il avait de plus cher au monde.
Ah! s'il eût pu se douter de cela quand Martial était debout devant lui, à portée de sa main, il eût fait payer cher au fils l'odieuse conduite du père.
Mais il se jurait bien que le châtiment n'était que différé.
Et ce n'était pas, de sa part, forfanterie de la colère.
Ce jeune homme si modeste et si doux avait un cœur inaccessible à la crainte.
Ses beaux yeux noirs et profonds, qui avaient la timidité tremblante des yeux d'une jeune fille, savaient aller droit à l'ennemi comme une lame d'épée.
Lorsque monsieur Lacheneur eut terminé par la dernière phrase qu'il avait adressée au duc de Sairmeuse, Monsieur d'Escorval lui tendit la main.
Je vous ai dit jadis que j'étais votre ami, prononçat-il d'une voix émue, je dois vous dire aujourd'hui que je suis fier d'avoir un ami tel que vous.
Le malheureux tressaillit au contact de cette main loyale qui lui était tendue, et son visage trahit une sensation d'une ineffable douceur.
Si mon père n'eût pas rendu, murmura l'opiniâtre Marie-Anne, mon père n'eût été qu'un dépositaire infidèle...
un voleur.
Il a fait son devoir.
Monsieur d'Escorval se retourna, un peu surpris, vers la jeune fille.
Vous dites vrai, mademoiselle, fit-il d'un ton de reproche; mais lorsque vous aurez mon âge et mon expérience, vous saurez que l'accomplissement d'un devoir est, en certaines circonstances, un héroïsme dont peu du gens sont capables.
Monsieur Lacheneur s'était redressé.
Ah!...
vos paroles me font du bien, monsieur le baron, dit-il, maintenant je suis content d'avoir agi comme je l'ai fait.
La baronne d'Escorval se leva, trop femme pour savoir résister aux généreuses inspirations de son cœur.
Moi aussi, monsieur Lacheneur, prononça-t-elle, je veux vous serrer la main.
Je veux vous dire que je vous estime autant que je méprise les tristes ingrats qui ont essayé de vous humilier alors qu'ils devaient tomber à vos pieds...
Vous avez rencontré des monstres sans cœur, tels qu'on ne trouverait sans doute pas leurs semblables.
Hélas! soupira le baron, les alliés nous en ont ramené comme cela quelques-uns qui pensent que le monde a été créé pour eux.
Et ces gens-là, gronda Lacheneur, voudraient être nos maîtres!...
La fatalité voulut que personne n'entendît monsieur Lacheneur.
Questionné sur le sens de sa phrase, il eût sans doute laissé deviner quelque chose des projets dont le germe existait déjà dans son esprit...
Et alors, que de catastrophes évitées!...
Cependant Monsieur d'Escorval reprenait peu à peu son sang-froid.
Maintenant, mon cher ami, demanda-t-il, quelle conduite vous proposez-vous de tenir avec les messieurs de Sairmeuse?
Ils n'entendront plus parler de moi...
d'ici quelque temps du moins.
Quoi!...
vous ne réclamerez pas les dix mille francs qu'ils vous doivent?...
Je ne demanderai rien...
Il le faut pourtant, malheureux.
Puisque vous avez parlé du legs de dix mille francs de votre marraine, votre honneur exige que vous en poursuiviez par tous les moyens légaux la restitution...
Il y a encore des juges en France...
Monsieur Lacheneur hocha la tête.
Les juges, fît-il, ne m'accorderaient pas la justice que je veux; je ne m'adresserai pas à eux...
Cependant...
Non, monsieur, non, je ne veux plus avoir rien de commun avec ces nobles de malheur.
Je n'enverrai même pas chercher à leur château mes hardes et celles de ma fille.
S'ils me les renvoient...
bien.
S'il leur plait de les garder, tant mieux!
Plus leur conduite à mon égard sera honteuse, infâme, odieuse, plus je serai satisfait...
Le baron ne répliqua pas, mais sa femme prit la parole, ayant, croyait-elle, un moyen sûr de vaincre cette incompréhensible obstination.
Je comprendrais votre résolution, monsieur, dit-elle, si vous étiez seul au monde, mais vous avez des enfants...
Mon fils a dix-huit ans, madame, une bonne santé et de l'éducation...
il se tirera d'affaire tout seul à Paris, à moins qu'il ne préfère ici me seconder.
Mais votre fille?...
Marie-Anne restera près de moi.
Monsieur d'Escorval crut devoir intervenir.
Prenez garde, mon cher ami, dit-il, que la douleur ne vous égare.
Réfléchissez...
Que deviendrez-vous, votre fille et vous?...
Le pauvre dépossédé eut un sourire navrant.
Oh!...
répondit-il, nous ne sommes pas aussi dénués que je l'ai dit, j'ai exagéré.
Nous sommes propriétaires encore.
L'an dernier, une vieille cousine à moi, que je n'avais jamais pu déterminer à venir habiter Sairmeuse, est morte en nommant Marie-Anne héritière de tout son bien...
Tout son bien, c'était une méchante masure tout en haut de la lande de la Rèche, avec un petit jardin devant et quelques perches de mauvais terrain.
Cette masure, je l'ai fait réparer sur les prières de ma fille, et j'y ai fait même porter quelques meubles, deux mauvais lits, une table, quelques chaises...
Ma fille comptait y établir gratis, en manière de retraite, le père Grivat et sa femme...
Et moi, du sein de mon opulence, je disais.
Mais ils seront supérieurement là dedans, ces deux vieux, ils vivront comme des coqs en pâte!...
Eh bien! ce que je jugeais si bon pour les autres, sera bon pour moi...
Je cultiverai des légumes et Marie-Anne ira les vendre...
Parlait-il sérieusement?
Maurice le crut, car il s'avança brusquement au milieu du salon.
Cela ne sera pas, monsieur Lacheneur, s'écria-t-il.
Oh!...
Non, cela ne sera pas, parce que j'aime Marie-Anne et que je vous la demande pour femme.
fin du chapitre cinq de l'honneur du nom.
Enregistré par Ezwa en Belgique en janvier deux mille neuf.